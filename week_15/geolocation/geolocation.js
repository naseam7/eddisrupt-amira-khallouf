const express = require("express");
const fs = require("fs");
const app = express();
const port = 3000;
const fetch = require('node-fetch');

app.get('/', function (req, res) {
	res.sendFile(`${__dirname}/index.html`);
});

app.get('/map', function (req, res) {
	console.log("id: " + req.query.location);
	fetch(`https://nominatim.openstreetmap.org/search?q=${req.query.location}&format=json`)
		.then(res => res.json())
		.then(json => res.send({ lat: json[0].lat, lng: json[0].lon }));
});



app.listen(port, () => console.log(`Server started on port ${port}!`));

/* 404 */
app.get("*", function (req, res) {
	res.send("Something went wrong!", 404);
});

// Hello!
