const express = require("express");
const fetch  = require("node-fetch");

const app = express();
const port = 3000;

app.use(express.static("public"));

const bodyParser = require('body-parser');
app.use(bodyParser.json());

app.get("/", (req, res) => res.sendFile(__dirname + "/index.html"));

// this is extactly as:
// app.get("/", (req, res) => {res.send("Welcome to our Campaign"))};

/*
function (err) {
	console.error(err);
}

is the same as : err => console.error(err)

(err) => {
	return console.error(err);
}
*/

app.get("/users", (req, res) => {
	

	fetch('https://us20.api.mailchimp.com/3.0/lists/1d4a18f2eb/members', { // we can add 
		headers: {
			'Authorization': 'apikey a493ea081d40baa2aebefa7af4b4cf01-us20'
		}
	})
	.then(function(response) {
		return response.json()
	})
	.then(function(response){
		res.send(response.members)
	})
	.catch(err => console.error(err));
});



app.post("/users", (req, res) => {
	const data = {email_address: req.body.email, status: "subscribed"}
	
	console.log(req.body);  // body syands for the body of the HTTP requests
	
	fetch('https://us20.api.mailchimp.com/3.0/lists/1d4a18f2eb/members', {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Authorization': 'apikey a493ea081d40baa2aebefa7af4b4cf01-us20',
			'Content-Type': 'application/json'
		}
	})
	.then(function(response) {
		return response.json()
	})
	.then(function(response){
		res.send("success!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
	})
	.catch(err => console.error(err));
});





app.listen(port, () => console.log(`Server started on port ${port}!`));



/* 404 */
app.get("*", function (req, res) {
	res.send("Something went wrong!", 404);
});

