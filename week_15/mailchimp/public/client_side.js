var userForm = document.getElementById("userForm");

userForm.addEventListener("submit", function(event){
    event.preventDefault()

   const data = {email:userForm.children[1].value} // we found [1] after i looked at the console and found an HTMLCollection and the input here has an index [1] Originally value is a method of the input
   fetch('/users', {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-Type': 'application/json'
		}
    })
    .then(response => response.text())
    .then(response => console.log(response))
    .catch(err => console.error(err))
})

const listButton =  document.getElementById("mailchimpListUsers")

listButton.addEventListener("click", function(event) {
    fetch("/users")   // fetch is an API and not a library...Here we are using AJAX to get data from the server side and show it on the browser: client side
    .then(response => response.json())
    .then(response => console.log(response))
    .catch(err => console.error(err))
})



