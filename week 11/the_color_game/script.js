function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min));
}

function generateRandomColor() {
	//generate random red, green and blue intensity
	let r = getRandomInt(0, 255);
	let g = getRandomInt(0, 255);
	let b = getRandomInt(0, 255);

	return "RGB(" + r + "," + g + "," + b + ")";
}

function toggleDifficulty() {
	if (isEasy) {
		isEasy = false;
	} else {
		isEasy = true;
	}
	console.log(isEasy);
}

function appendSquares(box) {
	for (let i = 0; i < 6; i++) {
        let square = document.createElement("div");
        square.style.backgroundColor = generateRandomColor();
        box.appendChild(square);
        
	}
}

function changeColors(box) {
      
    for(let i=0; i < box.childNodes.length; i++) {
        if(box.childNodes[i].nodeName === "DIV") {
            box.childNodes[i].style.backgroundColor = generateRandomColor()
        }
    }
}

// Difficulty
let isEasy = true;

let rgbValues = document.getElementsByClassName("RGB")[0];

rgbValues.textContent = generateRandomColor();

let difficultyButton = document.getElementById("btn").addEventListener("click", toggleDifficulty);

var squareBox = document.getElementsByClassName("squares")[0]; /* index[0] indicates to the the number of "the big container called squares that exist. We hae just one" */
appendSquares(squareBox);


 document.getElementById("right").addEventListener("click",function() {
    changeColors(squareBox);
});
