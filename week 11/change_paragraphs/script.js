// http://qnimate.com/generating-random-colors-in-javascript/ useful source
/* A function to return random number from min to max */
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min));
}

function generateRandomColor() {
	//generate random red, green and blue intensity
	var r = getRandomInt(0, 255);
	var g = getRandomInt(0, 255);
	var b = getRandomInt(0, 255);

	return "rgb(" + r + "," + g + "," + b + ")";
}

function changeColorsToElements(htmlCollectionForInstance) {
	for (var i = 0; i < elements.length; i++) {
		htmlCollectionForInstance[i].style.color = generateRandomColor();
	}
}

var elements = document.getElementsByClassName("paragraphs_item");

changeColorsToElements(elements);
