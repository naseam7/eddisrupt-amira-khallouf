var fs = require("fs");

// Asynchronous read
setTimeout(() => {
   fs.writeFile('input.txt',"Hello Mundo!", function(err) {
      if (err) {
         return console.error(err);
      }
      console.log("Asynchronous write");
   });
}, 5000);
   



console.log("Program Ended");