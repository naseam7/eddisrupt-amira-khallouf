var fs = require("fs");

// Asynchronous read
setTimeout(function() {
	fs.writeFile("input.txt", "Hello Mundo!", function(err) {
		if (err) {
			return console.error(err);
		}
		console.log("File wrote");

		fs.readFile("input.txt", "utf-8", function(err, data) {
			if (err) {
				return console.error(err);
			}
			console.log(data);
		});
	});
}, 2000);
