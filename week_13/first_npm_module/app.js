var catMe = require('cat-me')
 
console.log(catMe()) // => returns a random cat
console.log(catMe("nyan")) 


let faker = require("faker")

for (let i= 0; i< 10; i++) {
    let randomNumber = Math.random() * 20;
    let randomName = faker.name.findName(); // findName() is a method created for using faker exclusively not like anyother any Javascript method
    console.log(randomName + '-$' + randomNumber.toFixed(2))
}