

var students_number = Number(prompt("How many students does the class have?"));


 

var students_list = [];
var sum = 0;

for (let i=0; i< students_number; i++) {
    students_list.push({
        name: prompt("What is the name of the student?"),
        grade: Number(prompt("What is the grade of the student?"))
    })
    sum += students_list[i].grade;
}

var average = sum / students_number;
var student_name = prompt('Whose variation do you need to know?');
var found = false;
var student_grade;
for (let x=0; x < students_list.length; x++){
    if (student_name === students_list[x].name){
        student_grade = students_list[x].grade
        found = true;
    }
}

if (found) {
    let difference = student_grade - average
    let student_variation = difference * 100 / average;
            if (student_variation > 0){
            alert(student_name +' variation from average is +' + student_variation + '%') 
        } else{
            alert(student_name +' variation from average is ' + student_variation + '%')   

        }
} else {
    alert('Sorry, we don’t have that student listed');
}
