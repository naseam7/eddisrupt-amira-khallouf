var width = Number(prompt("give the width of the tree"));
var asterisks = 1;
var white_spaces= width - asterisks;
while (asterisks <= width) {
    alert(" ".repeat(white_spaces/2) + '*'.repeat(asterisks) + " ".repeat(white_spaces/2));
    asterisks = asterisks + 2;
    white_spaces = width - asterisks;
}
function printTreeRowOnTheScreen (treeRow) {

    var node = document.createElement("pre");           	 
           var textnode = document.createTextNode(treeRow);
           node.appendChild(textnode);
           document.querySelector("body").appendChild(node);
    
}
    