const express = require("express");
const fs = require("fs");
const app = express();
const port = 3000;
const path = require("path");

// viewed at http://localhost:8080
app.get("/", function(req, res) {
	res.sendFile(path.join(__dirname + "/index.html"));
});


// app.get("/video", (req, res) => res.send("Welcome to the homepage"));

app.listen(port, () => console.log(`Server started on port ${port}!`));

app.get("/video", (req, res) => {
	fs.readFile("./sample.mp4", (err, data) => {
		if (err) throw err;

		res.send(data);
	});
});



app.get("/hello", function(req, res) {
	res.sendFile(path.join(__dirname + "/hello.html"));
});
// app.get("/japa, function(req, res) {

// 	console.log("someone entered the Japan_1 page")
// 	res.send("Tokyo");
// });

/*
app.get("/posts/:category", function(req, res) {
	var searchedCategory = req.params.category;
	// console.log(req);

	console.log(searchedCategory);

	for (let i = 0; i < post.length; i++) {
		if (searchedCategory === post[i].category) {
			res.write(
				post[i].title +
					post[i].content +
					post[i].author +
					post[i].category
			);
		}
	}
	res.end();
});


app.get("/posts", function(req, res) {
	var allPosts = req.params.post;
	// console.log(req);

	console.log(allPosts);

	for (let i = 0; i < post.length; i++) {
			res.write(
				post[i].title +
					post[i].content +
					post[i].author +
					post[i].category
			);
		}
	res.end();
});

// It is supposed to eb always at the end

*/

/* 404 */
app.get("*", function(req, res) {
	res.send("Something went wrong!", 404);
});
