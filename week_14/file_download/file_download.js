const express = require("express");
const app = express();
const port = 3000;
// const JSZip = require("jszip");
const fs = require("fs");
const zipdir = require('zip-dir');


zipdir('/path/to/be/zipped', function (err, buffer) {
  // `buffer` is the buffer of the zipped file
});
 
zipdir('/path/to/be/zipped', { saveTo: '~/myzip.zip' }, function (err, buffer) {
  // `buffer` is the buffer of the zipped file
  // And the buffer was saved to `~/myzip.zip`
});
 
// Use a filter option to prevent zipping other zip files!
// Keep in mind you have to allow a directory to descend into!
zipdir('/path/to/be/zipped', { filter: (path, stat) => !/\.zip$/.test(path) }, function (err, buffer) {
  
});
 
// Use an `each` option to call a function everytime a file is added, and receives the path
zipdir('/path/to/be/zipped', { each: path => console.log(p, "added!"), function (err, buffer) {
 
});
  





app.get("/", (req, res) => {
	/*
	const zip = new JSZip();

	const test = zip.folder("./test");

	test.generateAsync({ type: "base64", streamFiles: false }, function(
		metadata
	) {
		console.log(metadata);
	})
		.then(createdFile => {
			console.log(createdFile);
			fs.writeFile("./out.zip", createdFile, function() {
				console.log("everyrhing is ok");
			});
		})
		.catch(err => {
			console.log(err);
		});
		*/
	// test.generateNodeStream({ type: "nodebuffer", streamFiles: true })
	// 	.pipe(fs.createWriteStream("out.zip"))
	// 	.on("finish", () => {
	// 		console.log("asdfasdf");
	// 	});

	res.send("Welcome my friend!!!");
});

// define a route to download a file
app.get("/file", (req, res) => {
	res.download("./test/test.txt");
});

app.listen(port, () => console.log(`Server started on port ${port}!`));

// It is supposed to eb always at the end
/* 404 */
app.get("*", function(req, res) {
	res.send("Something went wrong!", 404);
});
