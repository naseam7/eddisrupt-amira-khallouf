
function Meteorite(weight, density, composition) {
    this.weight = weight;
    this.density = density;
    this.composition = composition;
}
var meteorite1 = new Meteorite( 350, 7, ['iron', "oxygen"]);
var meteorite2 = new Meteorite( 369, 7,['iron', "oxygen"]);
var meteorite3 = new Meteorite( 390, 9,['iron', "oxygen"]);
var meteorite4 = new Meteorite( 350, 4,['iron', "oxygen"]);


class Meteorite {
    constructor(weight, density, composition){
        this.weight = weight;
        this.density = density;
        this.composition = composition;
    }
}
var meteorite5 = new Meteorite( 350, 4, ['iron', "oxygen"]);
