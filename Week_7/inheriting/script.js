
 function SpaceObject(weight, km, time) {
    this.weight = weight;
    this.km = km;
    this.time = time;
 }

function Meteorite (weight, km, time){
    SpaceObject.call(this, weight, km, time)

}

function Planet (weight, km, time){
    SpaceObject.call(this, weight, km, time)

}
SpaceObject.prototype.getVelocity = function (){
        return this.km / this.time
    }
Meteorite.prototype = Object.create(SpaceObject.prototype);
Planet.prototype = Object.create(SpaceObject.prototype);


 var meteorite1 = new Meteorite(45, 100, 200000);
var planet1 = new Meteorite(75, 100, 200000);
planet1.getVelocity();


/*

class SpaceObject {
    constructor(weight, km, time) {
      this.weight = weight;
      this.km = km;
      this.time = time;
    }

    // Method
    getVelocity() {
      return this.weight / this.time;
    }
  }


class Meteorite extends SpaceObject { 
    constructor (weight, km, time) {
        super (weight, km, time);
    }
}

class Planet extends SpaceObject { 
    constructor (weight, km, time) {
        super (weight, km, time);
    }
}
 var meteorite1 = new Meteorite(45, 100, 200000);
 var planet1 = new Meteorite(75, 100, 200000);
 planet1.getVelocity();
 meteorite1.getVelocity();