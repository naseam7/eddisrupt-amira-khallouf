
function add (n1, n2, n3, n4, n5) {
    return n1+n2+n3+n4+n5
}


var addition1 = add.bind(null, 1); /* null refers here to the global object which is the window and we can use the keyword "this" as well in stead of "null" */
var addition2 = addition1.bind(null, 4);
var addition3 = addition2.bind(null, 5);
var addition4 = addition3.bind(null, 5);
var addition5 = addition4.bind(null, 6);


addition5();
/*
addition1();
addition2();
addition3();
addition4();
addition5();
*/




