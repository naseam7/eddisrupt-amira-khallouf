function Customer(name, balance) {
	/* when the parameter starts with a value 0, we do not need  */
	this.name = name;
	this.balance = balance;
	this.debt = 0;
	/* Best Practice: inside the constructor we should put the things that are static such as strings, numbers and booleans. If we have methods, it is better to put them outside the constructor and we use prototype to inherit the method.*/
}

function NormalCustomer(name, balance) {
	Customer.call(this, name, balance);
}

function VIPCustomer(name, balance) {
	Customer.call(this, name, balance);
}
Customer.prototype.buy = function() {

	return this.buy;
};

Customer.prototype.createDebt = function() {
	return this.createDeb;
};

Customer.prototype.robBank = function() {
	return this.robBank === false;
};

VIPCustomer.prototype = Object.create(Customer.prototype);
NormalCustomer.prototype = Object.create(Customer.prototype);

class Product {
	constructor(name, price, category, units) {
		this.name = name;
		this.price = price;
		this.category = category;
		this.units = units;
	}
}

class VIPProduct extends Product {
	constructor(name, price, category, units) {
		super(name, price, category, units);
	}
}

class NormalProduct extends Product {
	constructor(name, price, category, units) {
		super(name, price, category, units);
	}
}

let list_customers = [];

var vipCustomer1 = new VIPCustomer(
	prompt("customer name"),
	prompt("customer balance")
);
var vipCustomer2 = new VIPCustomer(
	prompt("customer name"),
	prompt("customer balance")
);
var normalCustomer1 = new NormalCustomer(
	prompt("customer name"),
	prompt("customer balance")
);
var normalCustomer2 = new NormalCustomer(
	prompt("customer name"),
	prompt("customer balance")
);

// let vipCustomer1 = { name: "Pedro", balance: "2000" };
// let vipCustomer2 = { name: "Amira", balance: "3000" };
// let normalCustomer1 = { name: "João", balance: "2000" };
// let normalCustomer2 = { name: "Adam", balance: "20900" };

list_customers.push(vipCustomer1);
list_customers.push(vipCustomer2);
list_customers.push(normalCustomer1);
list_customers.push(normalCustomer2);

var products_count = Number(
	prompt("how many products we have today?")
); /* prompt always gives a string by default even if it was a number like 34---it is "34"*/

let products_list = [];

for (let i = 0; i < products_count; i++) {
	let productName = prompt("What is the product name?");
	let productPrice = Number(prompt("What is the product price?"));
	let productCategory = prompt("What is the product category?");
	let productUnit = Number(prompt("What is the product unit?"));
	let productType = prompt("What is the product type?");

	if (productType === "Vip") {
		products_list.push(new VipProduct(productName, productPrice, productCategory, productUnit));
	} else {
		products_list.push(new NormalProduct(productName, productPrice, productCategory, productUnit));
	}
}

// var is function scoped and let and const is block scoped
alert("The store is open !");

openingStore();

// Function declarations

function openingStore() {
	while (true) {
		// true refers forever
		let nameOfCustomer = prompt("Welcome to the store, What is your name?");

		if (nameOfCustomer !== "store_closed*") {
            showProductsList();
			var nameOfProduct = prompt(
				"What is the name of the product you want to buy?"
            );
            buy(nameOfProduct, nameOfCustomer);
			console.log(nameOfCustomer);
			console.log(nameOfProduct);
		} else {
			return;
		}
	}
}


function buy(product_name, customer_name) {
  let customerObject = checkCustomer(customer_name);
  let productObject = checkProduct(product_name);
  if (productObject instanceof VIPProduct && customerObject instanceof NormalCustomer) {
      alert("You are not on the VIP list, sorry");
  } else if (productObject.units === 0) {
      alert("We ran out of " + productObject.name + ", sorry");
   } else if (productObject === undefined) {  // to check if sth does exit
    alert("We don’t have that");
  } else if (customerObject.balance < productObject.price) {
      alert("Your credit card does not work, you don’t have money");

  } else {
      customerObject.balance = customerObject.balance - productObject.price;
      productObject.units-- // productObject.units = productObject.units -1
      alert("Thank you for your purchase, bye");
  }

}

function showProductsList() {
    alert("list of products:");
    for(let i=0; i < products_list.length; i++) {
    
    alert(products_list[i].productName + products_list[i].productPrice + products_list[i].productCategory + products_list[i].productUnit);
    }

}

function checkCustomer(name_of_customer) {
    for (let i= 0; i < list_customers.length; i++) {
       if (name_of_customer === list_customers[i].name){
        return list_customers[i];
    }
    }
        
}

function checkProduct(product) {
    for (let i= 0; i < products_list.length; i++) {
        if(product === products_list[i].name) {
            return products_list[i];
        }
        
    }
}


// var question_to_customer = prompt("Welcome to the store, What is your name?");

// while (question != "store_closed") {
//         prompt("Welcome to the store, What is your name?");
// }

// var nameOfProduct = prompt("What is the name of the product you want to buy?");

// if (productType === "VIP" &&  )

// var productToBuy = prompt("")

// ................
