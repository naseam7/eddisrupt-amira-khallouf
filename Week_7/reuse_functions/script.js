
function greet(age, gender, name) {
    if (gender === "male"){
        if (age >= 40){
            alert("Hello Mr." + name)
        } else {
            alert("Hey " + name + " my boy")
        }
    } else {
        if (age >= 40) { 
            alert("Hello Mrs." + name)
        } else {
            alert("Hey " + name  + " my girl")
        }
    }
}


let greetAnAdultMale = greet.bind(null, 55,"male");
greetAnAdultMale("Luis");

let greetAYoungster = greet.bind(null, 20);
greetAYoungster( "male","Antonio");

// function greetAnAdultMale() {
   
    

// }

// function greetAYoungster() {
//     
// }
