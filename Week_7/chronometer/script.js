var i = 0;
 function Chronometer (){     /* we do not need to put time as a parameter in the () because we have value of time is 0 */
    this.time = 0;
 }

Chronometer.prototype.addTime = function (addedTime){
        this.time = this.time + addedTime
}

Chronometer.prototype.startTime = function (){
    setInterval(function(){

        this.addTime(1);

        console.log(this.time);
    
    }.bind(this), 1);
}

Chronometer.prototype.getTime = function (){
    alert(this.time);
}

var chronometer1 = new Chronometer ();
var chronometer2 = new Chronometer ();



chronometer1.startTime();
chronometer1.getTime();
