/*
Code of a previous challenge: email validation:
let email = prompt("Give an email");
email = email.trim();

    if (/^\w+@\w+([.-]?\w+)*\.\w{2,3}$/.test(email)){
        alert("The email is valid, proceed with the email:" + email);
    } else {
        alert('Please insert a valid email');
    }
*/
/*
    var users = [
        {
            username: "john",
            email: "ggg@ggg.hhh",
            password: "hhh"
        }
    ]
*/

//Every user has email, password, followers and following
class User {
	constructor(username, email, password) {
		this.email = email;
		this.username = username;
		this.password = password;
		this.follower = 0;
		this.following = 0;
	}
}
class Instagram {
	constructor() {
		(this.chooseMessage =
			"Choose an action: log in, sign up, exit, search,log out,follow"),
			(this.loggedIn = false),
			(this.users = []),
			(this.personLoggedIn = {});
	}

	hasUser(email) {
		for (let i = 0; i < this.users.length; i++) {
			if (this.users[i].email === email) {
				return true;
			}
		}
		return false;
	}

	isEmailValid(email) {
		if (/^\w+@\w+([.-]?\w+)*\.\w{2,3}$/.test(email)) {
			return true;
		} else {
			return false;
		}
	}

	getUser(email) {
		for (let i = 0; i < this.users.length; i++) {
			if (email === this.users[i].email) {
				/* here we do not need to put this.email rather we put email === ...etc because email is a parameter here */
				return this.users[i];
			}
		}
	}

	createUser(name, email) {}

	listUser(email) {
		this.getUser(email);
	}
	askUser() {
		while (true) {
			let input = prompt(
				"Choose an action: log in, sign up, exit, search,log out,follow"
			);
			debugger;
			switch (input) {
				case "exit":
					this.exit();
					return; /* we took out the word break; because break stops running the switch block. We used, instead, the word return because return allows to stop the function askUser() when the user chooses to exit , there is no need to keep asking the user to choose an option */
				case "sign up":
					this.signUp();
					break;
				case "log in":
					this.logIn();
					break;
				case "search":
					this.findUser();
					break;
				case "log out":
					this.logOut();
					break;
				case "follow":
					this.follow();
					break;
				default:
					alert("We don't have that option\n");
					break;
			}
		}
	}

	signUp() {
		if (this.loggedIn) {
			alert("log out first before you create a new account\n");
			/* return is intended to stop the function in case the user was loggedIn*/
		} else {
			let name = prompt("Give your name");

			let email = this.emailBusiness();
			if (email === "exit*") {
				return;
			}
			let password = prompt("Give your password");

			this.users.push(new User(name, email, password));

			alert(
				"Thank you for your registration, welcome!\n"
			); /*\n adds a new line after this line*/
		}
	}

	emailBusiness() {
		let email = prompt("Give your email");
		email = email.trim();
		if (email === "exit*") {
			return email;
		} else if (this.hasUser(email)) {
			alert("Sorry, that email is already taken\n");
			return this.emailBusiness();
		} else if (!this.isEmailValid(email)) {
			alert("Insert a valid email\n");
			email = email.trim();
			return this.emailBusiness();
		} else {
			return email;
		}
	}

	logIn() {
		if (this.loggedIn) {
			alert("You are already logged in\n");
		} else {
			let email = prompt("Give your email");
			email = email.trim(); // to remove white spaces we use a built in method trim
			let password = prompt("Give your password");
			if (!this.hasUser(email)) {
				alert("We don't have that account\n");
			} else {
				
					if (this.getUser(email).password !== password) {
						alert("The password is incorrect\n");
					} else {
						alert("Welcome, " + this.getUser(email).username + ".\n");
						this.loggedIn = true;
						this.personLoggedIn = this.getUser(email);
					}
			}
		}
	}

	logOut() {
		if (!this.loggedIn) {
			alert(
				"Sorry, you have to be logged in to use that functionality\n"
			);
		} else {
			alert("You logged out, see you later\n");
			this.loggedIn = false;
		}
	}

	follow() {
		if (!this.loggedIn) {
			alert("Sorry, you have to be logged in to use that functionality\n");
			//we can put here:     return;      ... it stops running the follow function when the condition is achieved
		} else {
		let emailToFollow = prompt(
			"what is the email of the person that you want to follow?"
		);
		emailToFollow = emailToFollow.trim();
		if (!this.hasUser(emailToFollow)) {
			alert("That user does not exist\n");
		} else {
			for (let i = 0; i < this.users.length; i++) {
				if (emailToFollow === this.users[i].email) {
					this.users[i].follower++;
					this.personLoggedIn.following++;
					alert("You now follow " + this.users[i].username + "\n");
				}
			}
		}
		}
	}

	exit() {
		alert("You left the program, bye");
	}

	findUser() {
		if (!this.loggedIn) {
			alert(
				"Sorry, you have to be logged in to use that functionality\n"
			);
		} else {
			let email = prompt("give the email of the user");
			email = email.trim();
			if (!this.hasUser(email)) {
				alert("We have no results for that query\n");
			} else {
				let foundUser = this.getUser(email);
				alert(
					foundUser.username +
						"\n" +
						foundUser.email +
						"\n" +
						"Followers: " +
						foundUser.follower +
						"\n" +
						"Following: " +
						foundUser.following +
						"\n"
				);
			}
		}
	}
}

/* sign up
    Ask the user for name ---> email ---> password

If the email is not valid you must tell the user “Insert a valid email” and ask the for the email again. Create a function that validate email. You can use the function you create in a previous challenge ! If the email is valid, proceed.
Check if the email is already taken by another user. If that’s the case, denied the action and ask for the email again, saying “Sorry, that email is already taken” 
If there’s another user logged in that moment, the action is refused - “log out first before you create a new account”. Ask for the email again
If the user is successfully signs up - “Thank you for your registration, welcome!”
To avoid being stuck here → if the input of the email is “exit*”, it quits the sign up process.

*/

/*Log in

How log in works:

Ask for the email and password
If that email does not exist in the “database” the log in is refused with - “We don’t have that account”
If the password is incorrect, refuse the log in with - “The password is incorrect”
If there’s another user logged in that moment, the action is refused - “You are already logged in”.
If the log in is successful say - “Welcome <name>”
*/

/*
              log in
                var email = prompt("Give your email");
                var password = prompt("Give your password");
              if (!email) {
                alert("We don’t have that account");
                
              } 
              if 
                alert("The password is incorrect");
              
              
            }
            if user logged in
              alert("You are already logged in");
            
          }
          
          if logged in {
            
            alert("Welcome <name>")
          }

*/

let insta = new Instagram();

insta.askUser();
